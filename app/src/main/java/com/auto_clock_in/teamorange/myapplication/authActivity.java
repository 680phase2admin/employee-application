package com.auto_clock_in.teamorange.myapplication;

import android.content.Intent;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.Button;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.firebase.client.Firebase;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.FirebaseDatabase;

public class authActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText mEmailField;
    private EditText mPasswordField;
    private Button mLoginBtn;
    public static FirebaseAuth mAuth;
    Handler mHandler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auth);
        mAuth = FirebaseAuth.getInstance();
        mEmailField = (EditText) findViewById(R.id.emailField);
        mPasswordField = (EditText) findViewById(R.id.passwordField);
        mLoginBtn = (Button) findViewById(R.id.loginbtn);
        mLoginBtn.setOnClickListener(this);
        if (mAuth.getCurrentUser() != null) {
            finish();
            startActivity(new Intent(this, SelectCompany.class));
        }

    }

    @Override
    public void onClick(View v) {
        if (v == mLoginBtn) {
            String email = mEmailField.getText().toString();
            String password = mPasswordField.getText().toString();
            try {
                mAuth.signInWithEmailAndPassword(email, password);
                mHandler.postDelayed(mLaunchTask,1000);
                Toast.makeText(this, "Loading!", Toast.LENGTH_SHORT).show();
            } catch (Exception e) {
                Toast.makeText(this, "Add the correct credentials!", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private Runnable mLaunchTask = new Runnable() {
        public void run() {
            startActivity(new Intent(authActivity.this, SelectCompany.class));
            finish();
        }
    };


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
