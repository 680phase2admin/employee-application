package com.auto_clock_in.teamorange.myapplication;

import android.util.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Pavan on 4/21/2017.
 */

public class coordinates {

    public static List<String> listOfCoordinates = new ArrayList<>();

    private String lats;
    private String lngs;


    public String getValue() {
        lats =  value.substring(value.indexOf("latitude=")+9,value.indexOf(",")-1);
        lngs =  value.substring(value.indexOf("longitude=")+10,value.indexOf("}")-1);
        listOfCoordinates.add(lats);
        listOfCoordinates.add(lngs);
        return value;
    }

    public List<String> getListOfCoordinates(){
        return listOfCoordinates;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String value;
}
