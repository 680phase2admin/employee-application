package com.auto_clock_in.teamorange.myapplication;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.AdapterView;
import android.widget.Toast;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class SelectCompany extends AppCompatActivity {

    public Firebase mRef;
    public ArrayList<String> mCompanies = new ArrayList<>();
    public ListView mListView;
    public Map<String,String> mapList = new HashMap<>();
    public static coordinates coords = new coordinates();
    public static Firebase temp;
    public String email;
    public String name;
    public Handler mHandler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_company);
        Firebase.setAndroidContext(this);
        mRef = new Firebase("https://phase2-28a01.firebaseio.com/");
        email = authActivity.mAuth.getCurrentUser().getEmail();

        mListView = (ListView) findViewById(R.id.companyList);
        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, mCompanies);
        mListView.setAdapter(arrayAdapter);
        mRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Log.v("no of children ", "" + dataSnapshot.getChildrenCount());
                for (DataSnapshot childsnapShot : dataSnapshot.getChildren()) {
                    String clubkey = childsnapShot.getKey();
                    Log.v("company", "" + clubkey);
                    mCompanies.add(clubkey);
                }
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {
                Log.e("The read failed: ", firebaseError.getMessage());
            }
        });

         mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
         @Override
         public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
             Log.v("selected values is", "" + mListView.getItemAtPosition(position));
             temp =mRef.child(mListView.getItemAtPosition(position).toString());
             mRef.child(mListView.getItemAtPosition(position).toString()).child("Coordinates").addValueEventListener(new ValueEventListener() {
                 @Override
                 public void onDataChange(DataSnapshot dataSnapshot) {
                 for (DataSnapshot childsnapShot : dataSnapshot.getChildren()) {
                         coords.setValue(childsnapShot.getValue().toString());
                         Log.v("coordinates", "" + coords.getValue());
                     }
                     MainActivity.getCoordinates = coords.getListOfCoordinates();

                 }

                 @Override
                 public void onCancelled(FirebaseError firebaseError) {

                 }
             }


             );
             mRef.child(mListView.getItemAtPosition(position).toString()).child("Wifi SSID").addValueEventListener(new ValueEventListener() {
                 @Override
                 public void onDataChange(DataSnapshot dataSnapshot) {
                         Log.v("Wifi SSID", "" + dataSnapshot.getValue().toString());
                         MainActivity.wifi=dataSnapshot.getValue().toString();
                 }

                 @Override
                 public void onCancelled(FirebaseError firebaseError) {

                 }
             });

                 mRef.child(mListView.getItemAtPosition(position).toString()).child("Employees").addValueEventListener(new ValueEventListener() {
                 @Override
                 public void onDataChange(DataSnapshot dataSnapshot) {
                     for (DataSnapshot empsnapShot : dataSnapshot.getChildren()) {
                         String tmpString = empsnapShot.getValue().toString();
                         mapList.put(tmpString.substring(tmpString.indexOf("Email=")+6,tmpString.indexOf("Name")-2),
                                 tmpString.substring(tmpString.indexOf("Name=")+5,tmpString.indexOf("}")));
                     }
                 }

                 @Override
                 public void onCancelled(FirebaseError firebaseError) {

                 }
             });
             mHandler.postDelayed(mLaunchTask, 1000);
             coordinates.listOfCoordinates.clear();
         }

     }

);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_company, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //Add menu handling code
        switch (id) {
            case R.id.logOut:
                authActivity.mAuth.signOut();
                startActivity(new Intent(this, authActivity.class));
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private Runnable mLaunchTask = new Runnable() {
        public void run() {
            if(mapList.containsKey(email)){
                startActivity(new Intent(SelectCompany.this, MainActivity.class));
                MainActivity.nameEndPoint = mapList.get(email);
                finish();
                mapList.clear();
            } else {
                Toast.makeText(SelectCompany.this,"You are not authorized to work for this office!",Toast.LENGTH_SHORT).show();
                coordinates.listOfCoordinates.clear();
                mapList.clear();
            }
        }
    };

}
