package com.auto_clock_in.teamorange.myapplication;

import android.annotation.TargetApi;
import android.app.ActivityManager;
import android.app.Dialog;

import java.io.File;
import java.util.Locale;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.client.ChildEventListener;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polygon;
import com.google.android.gms.maps.model.PolygonOptions;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.firebase.auth.FirebaseUser;
import com.google.maps.android.PolyUtil;

import android.speech.tts.TextToSpeech;

import java.io.IOException;
import java.sql.Time;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Timer;

public class MainActivity extends AppCompatActivity
        implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, OnMapReadyCallback {

    private static final int ERROR_DIALOG_REQUEST = 9001;
    private Polygon shape;

    private int logFlag = 0;
    public StopWatch timer = new StopWatch();
    public static String totalDuration;
    public long duration;
    TTSManager ttsManager = null;
    Intent intent = new Intent();

    public static GoogleMap mMap;
    public GoogleApiClient mLocationClient;
    public LocationListener mListener;
    public static List<String> getCoordinates = new ArrayList<>();
    public static String wifi;
    public long firebaseTime;
    public static Firebase mTemp;
    public long firebaseDuration;
    public static String nameEndPoint;
    public Handler mHandler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (servicesOK()) {
            setContentView(R.layout.activity_map);
            initMap();
        } else {
            setContentView(R.layout.activity_main);
        }
        ttsManager = new TTSManager();
        ttsManager.init(this);
        mTemp = SelectCompany.temp.child("Employees/" + nameEndPoint + "/Work_hours");
        Toast.makeText(this, "Welcome " + nameEndPoint + "!", Toast.LENGTH_SHORT).show();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //Add menu handling code
        switch (id) {
            case R.id.mapTypeNormal:
                mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                break;
            case R.id.mapTypeSatellite:
                mMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
                break;
            case R.id.mapTypeTerrain:
                mMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
                break;
            case R.id.mapTypeHybrid:
                mMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
                break;
            case R.id.logOut:
                if (logFlag != 0) {
                    logFlag = 0;
                    timer.stop();
                    duration += timer.getElapsedTimeSecs();
                    mTemp.addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            firebaseTime = (long) dataSnapshot.getValue();
                        }

                        @Override
                        public void onCancelled(FirebaseError firebaseError) {

                        }
                    });
                    mHandler.postDelayed(mLaunchTask, 1000);
                }
                else {
                    LocationServices.FusedLocationApi.removeLocationUpdates(
                            mLocationClient, mListener
                    );
                    authActivity.mAuth.signOut();
                    startActivity(new Intent(MainActivity.this, authActivity.class));
                    finish();
                }

        }


        return super.onOptionsItemSelected(item);
    }

    public boolean servicesOK() {

        int isAvailable = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);

        if (isAvailable == ConnectionResult.SUCCESS) {
            return true;
        } else if (GooglePlayServicesUtil.isUserRecoverableError(isAvailable)) {
            Dialog dialog =
                    GooglePlayServicesUtil.getErrorDialog(isAvailable, this, ERROR_DIALOG_REQUEST);
            dialog.show();
        } else {
            Toast.makeText(this, "Can't connect to mapping service", Toast.LENGTH_SHORT).show();
        }

        return false;
    }

    private void initMap() {
        SupportMapFragment mapFragment =
                (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

    }

    @Override
    public void onMapReady(final GoogleMap googleMap) {
        mMap = googleMap;
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            permissionHandler();
            return;
        }
        mMap.setMyLocationEnabled(true);
        mLocationClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();

        mLocationClient.connect();

        drawPolygons();
    }

    private void gotoLocation(double lat, double lng, float zoom) {
        LatLng latLng = new LatLng(lat, lng);
        CameraUpdate update = CameraUpdateFactory.newLatLngZoom(latLng, zoom);
        mMap.animateCamera(update);
    }

    private void drawPolygons() {
        PolygonOptions options = new PolygonOptions()
                .fillColor(0x330000ff)
                .strokeWidth(3)
                .strokeColor(Color.BLUE);
        for (int i = 0; i < getCoordinates.size(); i += 2) {
            LatLng latlng = new LatLng(Double.parseDouble(String.valueOf(getCoordinates.get(i))), Double.parseDouble(String.valueOf(getCoordinates.get(i + 1))));
            options.add(latlng);
        }

        shape = mMap.addPolygon(options);
    }

    public void isInside() {
        if (shape.getPoints().size() == 0) {
            return;
        } else {
            if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                permissionHandler();
                return;
            }
            Location currentLocation = LocationServices.FusedLocationApi
                    .getLastLocation(mLocationClient);
            if (currentLocation == null) {
                Toast.makeText(this, "Couldn't connect!", Toast.LENGTH_SHORT).show();
            } else {
                LatLng latLng = new LatLng(
                        currentLocation.getLatitude(),
                        currentLocation.getLongitude()
                );
                List<LatLng> points = shape.getPoints();
                if (PolyUtil.containsLocation(latLng, points, true) || (Objects.equals(getWifiName(this), wifi))) {
                    if (logFlag != 1) {
                        logFlag = 1;
                        timer.start();
                        Toast.makeText(this, "Clocked in!", Toast.LENGTH_SHORT).show();
                        ttsManager.initQueue("Clocked in!");
                    }
                } else {
                    if (logFlag != 0) {
                        logFlag = 0;
                        timer.stop();
                        duration += timer.getElapsedTimeSecs();
                        mTemp.addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                firebaseTime = (long) dataSnapshot.getValue();
                            }

                            @Override
                            public void onCancelled(FirebaseError firebaseError) {

                            }
                        });
                        mHandler.postDelayed(mClockoutTask, 1000);
                    }
                }
            }
        }
    }

    public String getWifiName(Context context) {
        WifiManager wifiManager = (WifiManager) context.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        WifiInfo wifiInfo = wifiManager.getConnectionInfo();
        String ssid = wifiInfo.getSSID();
        return ssid.substring(1, ssid.length() - 1);
    }

    public void permissionHandler() {
        Toast.makeText(this, "Please add location permissions!", Toast.LENGTH_SHORT).show();
        intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", getPackageName(), null);
        intent.setData(uri);
        startActivity(intent);
        finish();
        return;
    }

    public String secondsToTimeStamp(long totalSecs) {
        long hours = totalSecs / 3600;
        long minutes = (totalSecs % 3600) / 60;
        long seconds = totalSecs % 60;
        return String.format("%02d:%02d:%02d", hours, minutes, seconds);
    }

    private Runnable mLaunchTask = new Runnable() {
        public void run() {
            firebaseDuration = duration + firebaseTime;
            mTemp.setValue(firebaseDuration);
            totalDuration = secondsToTimeStamp(firebaseDuration);
            Toast.makeText(MainActivity.this, "Clocked out!", Toast.LENGTH_SHORT).show();
            ttsManager.initQueue("Clocked out!");
            Toast.makeText(MainActivity.this, "Worked for: " + totalDuration + "seconds", Toast.LENGTH_SHORT).show();
            LocationServices.FusedLocationApi.removeLocationUpdates(
                    mLocationClient, mListener
            );
            authActivity.mAuth.signOut();
            startActivity(new Intent(MainActivity.this, authActivity.class));
            finish();
        }
    };

    private Runnable mClockoutTask = new Runnable() {
        public void run() {
            firebaseDuration = duration + firebaseTime;
            mTemp.setValue(firebaseDuration);
            totalDuration = secondsToTimeStamp(firebaseDuration);
            Toast.makeText(MainActivity.this, "Clocked out!", Toast.LENGTH_SHORT).show();
            ttsManager.initQueue("Clocked out!");
            Toast.makeText(MainActivity.this, "Worked for: " + totalDuration + "seconds", Toast.LENGTH_SHORT).show();
        }
    };

    @Override
    public void onConnected(Bundle bundle) {
        Toast.makeText(this, "Ready to map!", Toast.LENGTH_SHORT).show();
        ttsManager.initQueue("Ready to map!");
        mListener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                gotoLocation(location.getLatitude(), location.getLongitude(), 18);
                isInside();
            }
        };

        LocationRequest request = LocationRequest.create();
        request.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        request.setInterval(1000);
        request.setFastestInterval(100);

        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            permissionHandler();
            return;
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(
                mLocationClient, request, mListener
        );
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (logFlag != 0) {
            logFlag = 0;
            timer.stop();
            duration += timer.getElapsedTimeSecs();
            mTemp.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    firebaseTime = (long) dataSnapshot.getValue();
                }

                @Override
                public void onCancelled(FirebaseError firebaseError) {

                }
            });
            mHandler.postDelayed(mLaunchTask, 1000);
        }
        else {
            LocationServices.FusedLocationApi.removeLocationUpdates(
                    mLocationClient, mListener
            );
            authActivity.mAuth.signOut();
            startActivity(new Intent(MainActivity.this, authActivity.class));
            finish();
        }
    }
}